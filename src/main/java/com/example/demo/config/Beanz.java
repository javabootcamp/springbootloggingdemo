package com.example.demo.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Beanz {

    private static Logger logger = LoggerFactory.getLogger(Beanz.class);

    public String stringBean(){

        logger.info("Creating of the bean");

        return "";
    }

}
