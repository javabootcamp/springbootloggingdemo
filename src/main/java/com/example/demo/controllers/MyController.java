package com.example.demo.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Arvis Taurenis on 9/25/2018.
 */
@RestController
public class MyController {

    private static Logger logger = LoggerFactory.getLogger(MyController.class);

    @RequestMapping("/")
    public String test(){
        logger.trace("This is trace message");
        logger.debug("This is debug message");
        logger.info("This is info message");
        logger.warn("This is warn message");
        logger.error("This is error message");

        try{
            throw new IllegalArgumentException("Test exception");
        } catch (IllegalArgumentException e){
            logger.error("This was supposed to happen",e);
        }

        return "";
    }
}
